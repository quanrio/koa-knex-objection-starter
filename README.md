# Getting Started

Install `postgresql` and create a database, alternatively use any supported SQL database

Copy `.env.sample` to `.env` and populate all variables

Run following:

```
nvm install 12.16.2
npm install
node index.js
```

# Directory Structure

## src/migrations

Contains Knex migrations for database

### Example Migration File

```
exports.up = function (knex) {
  return knex.schema
  .createTable('user', function(table) {
    table.increments('id');
    table.string('first_name', 255).notNullable();
    table.string('last_name', 255).notNullable();
    table.string('username', 255).notNullable();
    table.string('display_image_url', 255);
    table.timestamps(false, true);
    table.unique(['name']);
  })
}

exports.down = function(knex) {
  return knex.schema
  .dropTable('user')
}
```

## src/seeds

Contains Knex seeds for database

### Example Seed File

```
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('user').del()
    .then(function (res) {
      return knex('profile').returning('*').insert([
        {
          first_name: 'John',
          last_name: 'Doe',
          username: 'johndoe'
        },
        {
          first_name: 'Jane',
          last_name: 'Doe',
          username: 'janedoe'
        }
      ]);
    })
};
```

## src/lib

Contains libraries for app wide usage.

- `src/lib/database.js` wrapper library for database connection and ORM model retreivals

## src/models

Contains Objection ORM models as modules

## src/routes

Contains koa routes (`Router.routes()`) as modules.

`src/routes/index.js` explicitly imports all routes modules and uses them under appropriate parent router. eg: `privateRouter.use(userRoutes)`.

### Example route

```
router.post('/register', async (ctx) => {
  const user = ctx.state.user;
  const inviteToken = ctx.request.body.token;

  ctx.body = await User.query().insert({
    email: user.email,
    first_name: ctx.request.body.first_name,
    last_name: ctx.request.body.last_name,
    phone: ctx.request.body.phone,
    user_id: user.uid
  });
})
```

## src/middlewares

Contains koa router middlewares as modules.

## src/email-templates

Contains all email templates

## src/server.js

### init()

Creates the koa app, uses API-wide middlewares, index routes and starts listening on environment set port.

## index.js

Requires server and `init()`s. Shall be used for any prestart functionality.

## knexfile.js

Responsible for knex configuration, uses database credentials from env.

## ecosystem.config.js

Configuration file for PM2 process manager.