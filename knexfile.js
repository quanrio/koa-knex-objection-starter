require('dotenv').config({});

module.exports = {
  client: 'pg',
  connection: {
    host: process.env.DATABASE_HOST,
    database: process.env.DATABASE_NAME,
    user:     process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    ssl: true
  },
  pool: {
    min: 2,
    max: 10,
    afterCreate: function(conn, done) {
      console.log('# Database Connected')
      done()
    }
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: './src/migrations'
  },
  seeds: {
    directory: './src/seeds'
  }
}
