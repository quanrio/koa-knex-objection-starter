const knex = require('knex');
const config = require('../../knexfile');
const { Model } = require('objection');

var dbInstance;

/**
 * create knex instance, run a test query and return instance
 * @returns {Knex}
 */

const getInstance = () => {
  if(!dbInstance) {
    dbInstance = knex(config);
    let query = dbInstance.raw('select 1+1 as result');
  }

  return dbInstance;
}

const getModel = () => {
  Model.knex(getInstance())
  return Model
}

module.exports = {
  getInstance, getModel
}