/**
 * Koa Middleware: parse authorization token, validate and 
 * @param {*} ctx 
 * @param {*} next 
 * @returns {Promise}
 */

const Auth = async function(ctx, next) {
	let token = ctx.headers.authorization;
	console.log('token', token);

	if(!token) {
		ctx.throw(403, JSON.stringify({
			message: 'Authorization token is missing'
		}));
	}

	token = token.split(' ');
	
	if (token[0].toLowerCase() !== 'bearer') {
		ctx.throw(403, JSON.stringify({
			message: 'Authorization token must be of type bearer'
		}));
	}

	else {
		token = token[1];
	}

	try {
        console.log('Validation');
        // perform token validation and setting user into req.state.user
	} catch(e) {
		ctx.throw(403, JSON.stringify({message: e.message}));
	}
}

module.exports = { Auth }