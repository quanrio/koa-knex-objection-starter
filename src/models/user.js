const Model = require('../lib/database').getModel();

class User extends Model {
  static get tableName() {
    return 'user';
  }
}

module.exports = User;