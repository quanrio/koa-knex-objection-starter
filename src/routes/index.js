require('dotenv').config()
const Router = require('koa-router');

const userRoutes = require('./users');
const authRoutes = require('./auth');

var prettyHtml = require('json-pretty-html').default;

var rootRouter = new Router();
var publicRouter = new Router();
var privateRouter = new Router();

// all handlers must return promises
// async functions automatically return promise, but offer ugly code against convenience

const errorHandler = async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    console.error(err);
    ctx.body = err.message;
  }
}

/////////// Common middlewares
publicRouter.use(errorHandler);
privateRouter.use(errorHandler);
///////////

publicRouter.get('/', (ctx) => {
  ctx.is = 'html';
  ctx.body = prettyHtml(privateRouter.stack.map(i => ({path: i.path, methods: i.methods})));
});
publicRouter.use(authRoutes);
privateRouter.use(userRoutes);

rootRouter.use(publicRouter.routes());
rootRouter.use(privateRouter.routes());

module.exports = rootRouter;