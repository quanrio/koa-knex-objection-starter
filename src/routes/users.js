const Router = require('koa-router');

var router = new Router();

const mockUsers = [{
  username: 'johndoe'
}];

router.get('/users', async (ctx) => {
  ctx.body = mockUsers;
});

module.exports = router.routes();