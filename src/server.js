const APP_NAME = require(__dirname + '/../package.json').name;
const Koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const router = require('./routes');
const { getInstance } = require('./lib/database');

if (process.env.ENV !== 'production') {
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
}

var app = new Koa();
const PORT = process.env.PORT || 8080;

function init() {

  getInstance().raw('select 1+1 as result')
  .catch(err => {
    console.log(err);
    process.exit(1);
  });

  app
  .use(cors())
  .use(bodyParser())
  .use(router.allowedMethods())
  .use(router.routes());

  app.listen(PORT, function() {
    console.log(`${APP_NAME} API listening on port ${PORT}`);
  })
}

module.exports = {
  init
}